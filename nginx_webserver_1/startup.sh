#!/bin/bash

# Start php-fpm
/etc/init.d/php-fpm.sh start
status=$?
if [ $status -ne 0 ]; then
  echo "Failed to start php-fpm: $status"
  exit $status
fi

# Start nginx
/etc/init.d/nginx.sh start
status=$?
if [ $status -ne 0 ]; then
  echo "Failed to start nginx: $status"
  exit $status
fi
echo nameserver 172.17.0.1 >> /etc/resolv.conf
